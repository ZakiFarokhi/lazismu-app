import React, { useState, useEffect } from 'react'
import { Provider, Menu, Button } from 'react-native-paper'
import axios from 'axios'

const Kecamatan = (props) => {
    const [listKecamatan, setListKecamatan] = useState([])
    const [visible, setVisible] = useState(false);
    const [kecamatan, setKecamatan] = useState()
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);
    useEffect(() => {
        getListKecamatan()
    }, [props.kabupaten])

    const getListKecamatan = async () => {
        try {
            const response = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${props.kabupaten}`)
            if (response.status !== 200) {
                console.log(response.data)
            }
            setListKecamatan(response.data.kecamatan)

        } catch (err) {
            console.log("error provinsi ", err)
        }
    }

    return (
        <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
                <Button style={{
                    borderColor: '#000000',
                    borderBottomWidth: 1,
                }} onPress={openMenu}>{kecamatan ? kecamatan : "pilih Kecamatan"}</Button>
            }>
            {listKecamatan.map(result => {
                return <Menu.Item onPress={() => onSelect({ nama: result.nama, id: result.id })} key={result.id} title={result.nama} />
            })
            }
        </Menu>
    )
    function onSelect(object) {
        setKecamatan(object.nama)
        closeMenu()
        props.onKecamatanSelect(object)
    }

}
export default Kecamatan