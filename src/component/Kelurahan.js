import React, { useState, useEffect } from 'react'
import { Provider, Menu, Button } from 'react-native-paper'
import axios from 'axios'

const Kelurahan = (props) => {
    const [listKelurahan, setListKelurahan] = useState([])
    const [visible, setVisible] = useState(false);
    const [kelurahan, setKelurahan] = useState()
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);
    useEffect(() => {
        getListKelurahan()
    }, [props.kecamatan])

    const getListKelurahan = async () => {
        try {
            const response = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=${props.kecamatan}`)
            if (response.status !== 200) {
                console.log(response.data)
            }
            setListKelurahan(response.data.kelurahan)

        } catch (err) {
            console.log("error provinsi ", err)
        }
    }

    return (

        <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
                <Button style={{
                    borderColor: '#000000',
                    borderBottomWidth: 1,
                }} onPress={openMenu}>{kelurahan ? kelurahan : "pilih Kelurahan"}</Button>
            }>
            {listKelurahan.map(result => {
                return <Menu.Item onPress={() => onSelect({ nama: result.nama, id: result.id })} key={result.id} title={result.nama} />
            })
            }
        </Menu>

    )
    function onSelect(object) {
        setKelurahan(object.nama)
        closeMenu()
        props.onKelurahanSelect(object)
    }

}
export default Kelurahan