import React, { useState, useEffect } from 'react'
import { Provider, Menu, Button } from 'react-native-paper'
import axios from 'axios'

const Provinsi = (props) => {
    const [listProvinsi, setListProvinsi] = useState([])
    const [visible, setVisible] = useState(false);
    const [provinsi, setProvinsi] = useState("")

    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);
    useEffect(() => {
       getListProvinsi()
    }, [])

    const getListProvinsi = async () => {
        try {
            console.log("success")
            const response = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/provinsi`)
            if (response.status !== 200) {
                console.log(response.data)
            }
            console.log(response.data.provinsi)
            setListProvinsi([...listProvinsi, ...response.data.provinsi])
        } catch (err) {
            console.log("error provinsi ", err)
        }
    }

    return (
        <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
                <Button style={{
                    borderColor: '#000000',
                    borderBottomWidth: 1,
                }} onPress={openMenu}>{provinsi ? provinsi : "pilih Provinsi"}</Button>
            }>
            {listProvinsi.map(result => {
                return <Menu.Item onPress={() => onSelect({ nama: result.nama, id: result.id })} key={result.id} title={result.nama} />
            })
            }
        </Menu>
    )
    function onSelect(object) {
        setProvinsi(object.nama)
        closeMenu()
        console.log(object)
        props.onProvinsiSelect(object)
    }

}
export default Provinsi