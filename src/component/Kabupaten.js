import React, { useState, useEffect } from 'react'
import { Provider, Menu, Button } from 'react-native-paper'
import axios from 'axios'

const Kabupaten = (props) => {
    const [listKabupaten, setListKabupaten] = useState([])
    const [visible, setVisible] = useState(false);
    const [kabupaten, setKabupaten] = useState()
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);
    useEffect(() => {
        getListKabupaten()
    }, [props.provinsi])

    const getListKabupaten = async () => {
        try {
            const response = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${props.provinsi}`)
            if (response.status !== 200) {
                console.log(response.data)
            }
            setListKabupaten(response.data.kota_kabupaten)

        } catch (err) {
            console.log("error provinsi ", err)
        }
    }

    return (
        <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
                <Button style={{
                    borderColor: '#000000',
                    borderBottomWidth: 1,
                }} onPress={openMenu}>{kabupaten ? kabupaten : "pilih Kabupaten"}</Button>
            }>
            {listKabupaten.map(result => {
                return <Menu.Item onPress={() => onSelect({ nama: result.nama, id: result.id })} key={result.id} title={result.nama} />
            })
            }
        </Menu>
    )
    function onSelect(object) {
        setKabupaten(object.nama)
        closeMenu()
        props.onKabupatenSelect(object)
    }

}
export default Kabupaten