import * as React from 'react';
import { Appbar, Button, Menu, Divider, Provider } from 'react-native-paper';
import { View, StyleSheet, AsyncStorage } from 'react-native';

const AppBar = ({ navigation }) => {
    // const logOut = (navigation) => {
    //     AsyncStorage.clear();
    //     navigation.navigate("LoginScreen")
    // }
    
    return (
        <Appbar>
            {/* <Appbar.Action style={styles.Action} onPress={() => console.log("user")} icon="account-circle-outline" /> */}
            <Appbar.Action style={styles.Action} onPress={() => navigation.navigate("ListScreeb")} icon="file-document" />
            <Appbar.Action style={styles.Action} onPress={()=> navigation.navigate("LoginScreen")} icon="logout" />
        </Appbar>
    )

}


export default AppBar

const styles = StyleSheet.create({
    Action: {
        justifyContent: "center",
        flex: 0.33
    },
});