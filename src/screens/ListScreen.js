import React, { useState, useEffect } from "react";
import { Text, View, AsyncStorage, StyleSheet } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Axios from "axios";
import { SafeAreaView } from "react-native-safe-area-context";
import { ScrollView } from "react-native-gesture-handler";
import { Card, Button, FAB, Title, Paragraph, IconButton } from 'react-native-paper'
import AppBarComponent from "../component/AppBar";
import { Appbar, } from 'react-native-paper';


const ListScreen = ({ navigation }) => {
  const [collection, setCollection] = useState([]);
  const [user, setUser] = useState(AsyncStorage.getItem("userId"))

  useEffect(() => {
    getProposal()
  }, []);

  const getProposal = async () => {
    try {
      const userId = await AsyncStorage.getItem("userId");
      const response = await Axios.get(`https://express.lazismusemarang.org/proposal/find?userId=${userId}`)
      console.log(response.data)
      if (response.status !== 200) {
        console.log(response)
      }
      setCollection(response.data.data)
    } catch (err) {
      console.log(err)
    }
  }
  const logOut = async () => {
    try{
      AsyncStorage.removeItem("isLoggedIn")
      AsyncStorage.removeItem("userId")
      AsyncStorage.removeItem("password")
      navigation.navigate("LoginScreen")
    }catch(err){

    }
  }
  return (
    <View style={styles.container}>
      <Appbar>
        <Appbar.Action style={styles.Action} onPress={() => navigation.navigate("ListScreen")} icon="file-document" />
        <Appbar.Action style={styles.Action} onPress={() => logOut()} icon="logout" />
      </Appbar>
      <ScrollView style={styles.scrollView}>
        {
          collection.map(result => {
            return (
              <Card>
                <Card.Cover source={{ uri: result.imageLocal }} />
                <Card.Title title={result.nama}  />
                <Card.Content>
                  <Title>Nominal: Rp.{result.nominal}</Title>
                  <Paragraph>No hp: {result.nomorhp}</Paragraph>
                  <Paragraph>Alamat: {result.alamat}</Paragraph>
                  <Paragraph>Keterangan: {result.keterangan}</Paragraph>
                </Card.Content>
                <Card.Actions>
                  <Title style={{color:result.status?"green":"red"}}>{result.status ? "Di Setujui" : "Proses Survey"}</Title>
                </Card.Actions>
              </Card>
            )
          })
        }
      </ScrollView>
      <View style={styles.container}>

        <FAB
          style={styles.fab}
          small
          icon="plus"
          onPress={() => navigation.navigate("AddScreen")}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
  scrollView: {
    backgroundColor: "pink",
    marginHorizontal: 20,
    marginTop: 10,
  },
  text: {
    fontSize: 42,
  },
  Action: {
    justifyContent: "center",
    flex: 0.5
},
});

export default ListScreen;
