import React, { useState, useEffect } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Text,
  Button,
  Keyboard,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Image,
  Alert,
  AsyncStorage,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Axios from 'axios';

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  useEffect(()=> {
    redirect()
  }, [])

  const redirect = async () => {
    try{
      const isLoggedIn = await AsyncStorage.getItem("isLoggedIn")
      if(isLoggedIn == "true"){
        navigation.navigate("ListScreen")
      }
    }catch(err){
      console.log(err)
    }
  } 

  const LoginSubmit = async () => {
    try {
      console.log("login")
      const response = await Axios.post(
        'https://express.lazismusemarang.org/user/login',
        {email: email, password: password},
      );
      console.log(response)
      if(response.status !== 200){
        console.log(response.data.data)
      }
      console.log(response.data)
      const data = await response.data.data;
      AsyncStorage.setItem("isLoggedIn", "true")
      AsyncStorage.setItem("userId", `${data.id}`)
      AsyncStorage.setItem("password", response.data.data.password)
      navigation.navigate('ListScreen')
    } catch (err) {
      console.warn("itu ", err);
    }
  };
  const ShowAlert = (message) => {
    Alert.alert(
      'Alert',
      message,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log('Ok Pressed'),
        },
      ],
      {
        cancelable: false,
      },
    );
  };
  return (
    <KeyboardAwareScrollView style={styles.container}>
      <View style={styles.inner}>
        <Text
          style={styles.signup_text}
          onPress={() => navigation.navigate('SignupScreen')}>
          Signup
        </Text>
        <Image
          style={{ resizeMode: 'center' }}
          source={require('../assets/images/lazismu.png')}
        />
        <TextInput
          placeholder="Email"
          style={styles.textInput}
          onChangeText={(value) => setEmail(value)}
          value={email}
        />
        <TextInput
          placeholder="Password"
          keyboardType="visible-password"
          secureTextEntry={true}
          style={styles.textInput}
          onChangeText={(value) => setPassword(value)}
          value={password}
        />
        <View style={styles.btnContainer}>
          <Button color="green" title="Login" onPress={LoginSubmit} />
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};
const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: 'space-around',
  },
  header: {
    fontSize: 36,
    marginBottom: 48,
  },
  textInput: {
    borderColor: '#000000',
    borderBottomWidth: 1,
    marginRight: 25,
    marginLeft: 25,
    marginBottom: 40
  },
  signup_text: {
    textAlign: 'right',
    fontWeight: 'bold',
    fontSize: 16,
    borderStyle: 'solid',
  },
  btnContainer: {
    backgroundColor: 'white',
    borderRadius: 25,
  },
});

export default LoginScreen;
