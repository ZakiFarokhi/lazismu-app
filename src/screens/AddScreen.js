import React, { useState, useEffect } from 'react';
import {
    View,
    TextInput,
    StyleSheet,
    Text,
    Platform,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
    AsyncStorage
} from 'react-native';
import { Button } from 'react-native-paper'
import axios from 'axios';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
    launchCamera,
    launchImageLibrary
} from 'react-native-image-picker'
import Provinsi from '../component/Provinsi';
import Kabupaten from '../component/Kabupaten';
import Kecamatan from '../component/Kecamatan';
import Kelurahan from '../component/Kelurahan';


const AddScreen = ({ navigation }) => {
    const [nama, setNama] = useState("")
    const [alamat, setAlamat] = useState("")
    const [nomorhp, setNomorhp] = useState("")
    const [nominal, setNominal] = useState("")
    const [keterangan, setKeterangan] = useState("")
    const [nomorKtp, setNomorKtp] = useState("")
    const [image, setImage] = useState("")
    const [status, setStatus] = useState(false)
    const [userId, setUserId] = useState("")

    const [provinsi, setProvinsi] = useState({})
    const [kabupaten, setKabupaten] = useState({})
    const [kecamatan, setKecamatan] = useState({})
    const [kelurahan, setKelurahan] = useState({})

    const [filePath, setFilePath] = useState({});
    useEffect(() => {
        getUserId()
    }, [])
    
    const getUserId = async () => {
        try{
            const id = await AsyncStorage.getItem("userId")
            setUserId(id)
        }catch(err){
            console.log(err)
        }
    }

    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission',
                    },
                );
                // If CAMERA Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                return false;
            }
        } else return true;
    };

    const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'External Storage Write Permission',
                        message: 'App needs write permission',
                    },
                );
                // If WRITE_EXTERNAL_STORAGE Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                alert('Write permission err', err);
            }
            return false;
        } else return true;
    };

    const captureImage = async (type) => {
        let options = {
            mediaType: type,
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
            videoQuality: 'low',
            durationLimit: 30, //Video max duration in seconds
            saveToPhotos: true,
        };
        let isCameraPermitted = await requestCameraPermission();
        let isStoragePermitted = await requestExternalWritePermission();
        if (isCameraPermitted && isStoragePermitted) {
            launchCamera(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                    alert('User cancelled camera picker');
                    return;
                } else if (response.errorCode == 'camera_unavailable') {
                    alert('Camera not available on device');
                    return;
                } else if (response.errorCode == 'permission') {
                    alert('Permission not satisfied');
                    return;
                } else if (response.errorCode == 'others') {
                    alert(response.errorMessage);
                    return;
                }
                console.log('base64 -> ', response.base64);
                console.log('uri -> ', response.uri);
                console.log('width -> ', response.width);
                console.log('height -> ', response.height);
                console.log('fileSize -> ', response.fileSize);
                console.log('type -> ', response.type);
                console.log('fileName -> ', response.fileName);
                setFilePath(response);
            });
        }
    };

    const chooseFile = (type) => {
        let options = {
            mediaType: type,
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
        };
        launchImageLibrary(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                alert('User cancelled camera picker');
                return;
            } else if (response.errorCode == 'camera_unavailable') {
                alert('Camera not available on device');
                return;
            } else if (response.errorCode == 'permission') {
                alert('Permission not satisfied');
                return;
            } else if (response.errorCode == 'others') {
                alert(response.errorMessage);
                return;
            }
            console.log('base64 -> ', response.base64);
            console.log('uri -> ', response.uri);
            console.log('width -> ', response.width);
            console.log('height -> ', response.height);
            console.log('fileSize -> ', response.fileSize);
            console.log('type -> ', response.type);
            console.log('fileName -> ', response.fileName);
            setFilePath(response);
        });
    };
    const Submit = async () => {
        try{
            const data = new FormData();
            data.append("name", "avatar");
            data.append("image", {
                uri: filePath.uri,
                type: filePath.type,
                name: filePath.fileName
            })
            const config = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                },
                body: data,
            };
            const responseUpload = await axios.post("https://express.lazismusemarang.org/proposal/upload", data)
            if(responseUpload.status !== 200){
                console.log(responseUpload)
            }
            const response = await axios.post("https://express.lazismusemarang.org/proposal/create", {
                nama:nama,
                alamat: `${provinsi.nama}, ${kabupaten.nama}, ${kecamatan.nama}, ${kelurahan.nama}, ${alamat}`,
                nomorhp:nomorhp,
                nominal:nominal,
                keterangan:keterangan,
                image:responseUpload.data.data,
                imageLocal:filePath.uri,
                userId:userId
            })
            if(response.status !== 200){
                console.log(response)
            }
            if(response.status == 200){
                navigation.navigate("ListScreen")
            }
            
        }catch(err){

        }
        
    }
    return (

        <KeyboardAwareScrollView style={styles.container}>
            <View style={styles.inner}>
                <TextInput
                    placeholder="Nama"
                    style={styles.textInput}
                    onChangeText={(value) => setNama(value)}
                    value={nama}
                />
                <TextInput
                    placeholder="Nomor Handphone"
                    style={styles.textInput}
                    onChangeText={(value) => setNomorhp(value)}
                    value={nomorhp}
                />
                {/* <TextInput
                    placeholder="Nomor KTP"
                    style={styles.textInput}
                    onChangeText={(value) => setNomorKtp(value)}
                    value={nomorKtp}
                /> */}
                <Provinsi onProvinsiSelect={(value) => setProvinsi(value)} />
                <Kabupaten onKabupatenSelect={(value) => setKabupaten(value)} provinsi={provinsi ? provinsi.id : null} />
                <Kecamatan onKecamatanSelect={(value) => setKecamatan(value)} kabupaten={kabupaten ? kabupaten.id : null} />
                <Kelurahan onKelurahanSelect={(value) => setKelurahan(value)} kecamatan={kecamatan ? kecamatan.id : null} />
                <TextInput
                    placeholder="Detail Alamat"
                    style={styles.textInput}
                    onChangeText={(value) => setAlamat(value)}
                    value={alamat}
                />
                <TextInput
                    placeholder="Nominal Bantuan"
                    style={styles.textInput}
                    onChangeText={(value) => setNominal(value)}
                    value={nominal}
                />
                <TextInput
                    placeholder="Keterangan Bantuan"
                    style={styles.textInput}
                    onChangeText={(value) => setKeterangan(value)}
                    value={keterangan}
                />
                <Image
                    source={{ uri: filePath.uri }}
                    style={styles.imageStyle}
                />
                <Text style={styles.textStyle}>{filePath.uri}</Text>
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.buttonStyle}
                    onPress={() => captureImage('photo')}>
                    <Text style={styles.textStyle}>Ambil Gambar</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.buttonStyle}
                    onPress={() => chooseFile('photo')}>
                    <Text style={styles.textStyle}>Choose Image</Text>
                </TouchableOpacity>
                <Button style={{ backgroundColor: "green" }} title="Login" onPress={Submit}>Submit</Button>
            </View>
        </KeyboardAwareScrollView>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    inner: {
        padding: 24,
        flex: 1,
        justifyContent: 'space-around',
        marginRight: 25,
        marginLeft: 25,
    },
    header: {
        fontSize: 36,
        marginBottom: 48,
    },
    buttonStyle: {
        flexDirection: 'row',
        backgroundColor: '#DDDDDD',
        padding: 5,
    },
    textStyle: {
        padding: 10,
        color: 'black',
    },
    imageStyle: {
        width: 200,
        height: 200,
        margin: 5,
    },
    textInput: {
        borderColor: '#000000',
        borderBottomWidth: 1,

    },
    signup_text: {
        textAlign: 'right',
        fontWeight: 'bold',
        fontSize: 16,
        borderStyle: 'solid',
    },
    btnContainer: {
        backgroundColor: 'white',
        borderRadius: 25,
    },
});

export default AddScreen;
