import React, { useState } from 'react';
import {
  View,
  KeyboardAvoidingView,
  TextInput,
  StyleSheet,
  Text,
  Platform,
  TouchableWithoutFeedback,
  Button,
  Keyboard,
  Image,
  Alert,
  AsyncStorage,
} from 'react-native';
import Axios from 'axios';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const SignupScreen = ({ navigation }) => {
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [nomorhp, setNomorhp] = useState('');
  const [nomorKtp, setNomorKtp] = useState('');

  const LoginSubmit = async () => {
    
    try {
      console.log( { nama: nama, email: email, password: password, nomorhp: nomorhp, nomorktp: nomorKtp });
      const response = await Axios.post(
        'https://express.lazismusemarang.org/user/signup',
        { nama: nama, email: email, password: password, nomorhp: nomorhp, nomorktp: nomorKtp },
      );
      console.log(response)
      if (response.status !== 200) {
        console.log("no");
      }
      const data = await response.data.data;
      AsyncStorage.setItem("isLoggedIn", "true")
      AsyncStorage.setItem("userId", `${data.id}`)
      AsyncStorage.setItem("password", response.data.data.password)
      navigation.navigate('ListScreen')
    } catch (err) {
      console.log(err);
    }
  };
  const ShowAlert = () => {
    Alert.alert(
      'Alert',
      `ini email ${email.toString()}, ini password ${password.toString()}`,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  };

  return (
    <KeyboardAwareScrollView style={styles.container}>
      <View style={styles.inner}>
        <Text
          style={styles.signup_text}
          onPress={() => navigation.navigate('LoginScreen')}>
          Signup
        </Text>
        <Image
          style={{ resizeMode: 'center' }}
          source={require('../assets/images/lazismu.png')}
        />
        <TextInput
          placeholder="Nama"
          style={styles.textInput}
          onChangeText={(value) => setNama(value)}
          value={nama}
        />
        <TextInput
          placeholder="Email"
          style={styles.textInput}
          onChangeText={(value) => setEmail(value)}
          value={email}
        />
        <TextInput
          placeholder="Password"
          keyboardType="visible-password"
          secureTextEntry={true}
          style={styles.textInput}
          onChangeText={(value) => setPassword(value)}
          value={password}
        />
        <TextInput
          placeholder="Nomor Handphone"
          style={styles.textInput}
          onChangeText={(value) => setNomorhp(value)}
          value={nomorhp}
        />
        <TextInput
          placeholder="Nomor KTP"
          style={styles.textInput}
          onChangeText={(value) => setNomorKtp(value)}
          value={nomorKtp}
        />
        <View style={styles.btnContainer}>
          <Button color="green" title="Signup" onPress={LoginSubmit} />
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: 'space-around',
  },
  header: {
    fontSize: 36,
    marginBottom: 48,
  },
  textInput: {
    borderColor: '#000000',
    borderBottomWidth: 1,
    marginRight: 25,
    marginLeft: 25,
    marginBottom: 15,
  },
  signup_text: {
    textAlign: 'right',
    fontWeight: 'bold',
    fontSize: 16,
    borderStyle: 'solid',
  },
  btnContainer: {
    backgroundColor: 'white',
    borderRadius: 25,
  },
});

export default SignupScreen;
