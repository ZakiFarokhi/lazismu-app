import React, { useState, useEffect } from 'react'
import { StyleSheet, ScrollView, View } from 'react-native'
import { Text, Title, Menu, TextInput, Button, Provider } from 'react-native-paper'
import Swiper from 'react-native-swiper'
import axios from 'axios'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


const AddScreen = ({ navigation }) => {
    const [nama, setNama] = useState("")
    const [alamat, setAlamat] = useState("")
    const [nomorhp, setNomorhp] = useState("")
    const [nominal, setNominal] = useState("")
    const [keterangan, setKeterangan] = useState("")
    const [nomorKtp, setNomorKtp] = useState("")
    const [image, setImage] = useState("")
    const [status, setStatus] = useState(false)

    const [provinsi, setProvinsi] = useState(null)
    const [kabupaten, setKabupaten] = useState(null)
    const [kecamatan, setKecamatan] = useState(null)
    const [kelurahan, setKelurahan] = useState(null)

    const [listProvinsi, setListProvinsi] = useState([])
    const [listKabupaten, setListKabupaten] = useState([])
    const [listKecamatan, setListKecamatan] = useState([])
    const [listKelurahan, setListKelurahan] = useState([])
    useEffect(()=> {
        getListProvinsi()
    },[])

    const [visible, setVisible] = React.useState(false);
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);

    
    const getListProvinsi = async () => {
        
        try {
            console.log("success")
            const response = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/provinsi`)
            if (response.status !== 200) {
                console.log(response.data)
            }
            console.log(response.data.provinsi)
            setListProvinsi([...listProvinsi, ...response.data.provinsi])
        } catch (err) {
            console.log("error provinsi ",err)
        }
    }
    const getListKabupaten = async (idProvinsi) => {
        try {
            const response = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${idProvinsi}`)
            if (response.status !== 200) {
                console.log(response.data.data)
            }
            setListKabupaten(response.data.data)
        } catch (err) {
            console.log(err)
        }
    }
    const getListKecamatan = async (idKabupaten) => {
        try {
            const response = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${idKabupaten}`)
            if (response.status !== 200) {
                console.log(response.data.data)
            }
            setListKecamatan(response.data.data)
        } catch (err) {
            console.log(err)
        }
    }
    const getListKelurahan = async (idKecamatan) => {
        try {
            const response = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=${idKecamatan}`)
            if (response.status !== 200) {
                console.log(response.data.data)
            }
            setListKelurahan(response.data.data)
        } catch (err) {
            console.log(err)
        }
    }

    const Submit = async () => {
        // const response = await axios.post('http://express.lazismusemarang.org/proposal/create/',
        //     { nama: nama, alamat: alamat, nomorhp: nomorhp, nominal: nominal, keterangan: keterangan, image: image, status: status }
        // )
    }

    return (
        // <Provider>

        
        <KeyboardAwareScrollView>
            <View style={styles.inner}>
                <Title>Pengajuan Bantuan</Title>
                {/* <Swiper style={styles.wrapper} showsButtons={true}> */}
                    {/* <View style={styles.slide1}> */}
                        <Title>Data Pribadi</Title>
                        <TextInput
                            placeholder="Nama"
                            style={styles.textInput}
                            onChangeText={(value) => setNama(value)}
                            value={nama}
                        />
                        <TextInput
                            placeholder="Nomor Handphone"
                            style={styles.textInput}
                            onChangeText={(value) => setNomorhp(value)}
                            value={nomorhp}
                        />
                        <TextInput
                            placeholder="Nomor KTP"
                            style={styles.textInput}
                            onChangeText={(value) => setNomorKtp(value)}
                            value={nomorKtp}
                        />
                    {/* </View>
                    <View style={styles.slide2}> */}
                        <Menu
                            visible={listProvinsi && !provinsi ? true : false}
                            onDismiss={closeMenu}
                            anchor={
                                // <TextInput onPress={()=>{openMenu; getListProvinsi}} placeholder="Provinsi" />
                            <Button onPress={ () => {openMenu()} }>{provinsi?provinsi:"provinsi"}</Button>
                            }>
                            {() => {
                                console.log("ini", listProvinsi)
                                listProvinsi.map(result => {
                                    console.log(result)
                                    return (
                                        <Menu.Item onPress={() => { setProvinsi(result.nama); getListKabupaten(result.id) }} title={result.nama} />
                                    )
                                })
                            }}
                        </Menu>
                        <Menu
                            visible={getListKabupaten && visible && !kabupaten ? true : false}
                            onDismiss={closeMenu}
                            anchor={
                                <TextInput disabled onPress={openMenu} placeholder="Kabupaten" value={kabupaten} />
                            }>
                            {() => {
                                getListKabupaten.map(result => {
                                    return (
                                        <Menu.Item onPress={() => { setKabupaten(result.nama); getListKecamatan(result.id) }} title={result.nama} />
                                    )
                                })
                            }}
                        </Menu>
                        <Menu
                            visible={getListKecamatan && visible && !kecamatan ? true : false}
                            onDismiss={closeMenu}
                            anchor={
                                <TextInput disabled onPress={openMenu} placeholder="Kecamatan" value={kecamatan} />
                            }>
                            {() => {
                                getListKecamatan.map(result => {
                                    return (
                                        <Menu.Item onPress={() => { setKecamatan(result.nama) }} title={result.nama} />
                                    )
                                })
                            }}
                        </Menu>
                        <Menu
                            visible={getListKelurahan && visible && !kelurahan ? true : false}
                            onDismiss={closeMenu}
                            anchor={
                                <TextInput disabled onPress={openMenu} onPress placeholder="Kelurahan" value={kelurahan} />
                            }>
                            {() => {
                                getListKelurahan.map(result => {
                                    return (
                                        <Menu.Item onPress={() => { setKelurahan(result.nama); getListKabupaten(result.id) }} title={result.nama} />
                                    )
                                })
                            }}
                        </Menu>
                    {/* </View>
                    <View style={styles.slide3}>
                        <Text style={styles.text}>And simple</Text>
                    </View>
                </Swiper> */}
            </View>
        </KeyboardAwareScrollView>
        // </Provider>
    )
}
const styles = StyleSheet.create({
    wrapper: {},
    slide1: {
        flex: 1,
        justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#FFFFFF',
        marginLeft:25,
        marginRight:25
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    inner: {
        
        flex: 1,
        justifyContent: 'space-around',
    },
    header: {
        fontSize: 36,
        marginBottom: 30,
    },
    textInput: {
        borderColor: '#000000',
        backgroundColor:"white",
        //borderBottomWidth: 1,
        // marginRight: 25,
        // marginLeft: 25,
        marginBottom: 15,
    },
    signup_text: {
        textAlign: 'right',
        fontWeight: 'bold',
        fontSize: 16,
        borderStyle: 'solid',
    },
    btnContainer: {
        backgroundColor: 'white',
        borderRadius: 25,
    },
});
export default AddScreen