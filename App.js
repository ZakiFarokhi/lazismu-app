/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  StyleSheet, AsyncStorage,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import {createStackNavigator} from '@react-navigation/stack'
import {NavigationContainer} from '@react-navigation/native'
import ListScreen from './src/screens/ListScreen';
import { Provider } from 'react-native-paper'
import SignupScreen from './src/screens/SignupScreen';
import LoginScreen from './src/screens/LoginScreen';
import AddScreen from './src/screens/AddScreen';
import DetailScreen from './src/screens/DetailScreen';
import UserScreen from './src/screens/UserScreen';
import AppBar from './src/component/AppBar';

const Stack = createStackNavigator()
const App: () => React$Node = () => {
  return (
    <Provider>
    <NavigationContainer>
      <Stack.Navigator initialRouteName="LoginScreen">
        <Stack.Screen component={AppBar} name="AppBar"/>
        <Stack.Screen component={SignupScreen} name="SignupScreen" />
        <Stack.Screen component={LoginScreen} name="LoginScreen" />
        <Stack.Screen component={ListScreen} name="ListScreen" />
        <Stack.Screen component={AddScreen} name="AddScreen" />
        <Stack.Screen component={DetailScreen} name="DetailScreen" />
        <Stack.Screen component={UserScreen} name="UserScreen" />
      </Stack.Navigator>
    </NavigationContainer>
    </Provider>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
